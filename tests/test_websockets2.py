# WS client example

import asyncio
import websockets

async def hello():
    uri = "ws://localhost:8000/ws/annotations/"
    async with websockets.connect(uri) as websocket:
        while True:
            greeting = await websocket.recv()
            print(f"Output: {greeting}")
            await asyncio.sleep(1)

asyncio.get_event_loop().run_until_complete(hello())
