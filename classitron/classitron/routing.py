"""
This file tells Django Channels how to route requests (it's
basically the Channels equivalent of the regular urls.py).
It includes HTTP routes from urls.py by default.
"""
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import classitron.classify.routing

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AuthMiddlewareStack(
        URLRouter(
            classitron.classify.routing.websocket_urlpatterns
        )
    ),
})
