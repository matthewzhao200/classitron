"""
This is just boilerplate to define the name of the app (we use this name in settings.INSTALLED_APPS)
"""

from django.apps import AppConfig

class ClassifyConfig(AppConfig):
    name = 'classify'
