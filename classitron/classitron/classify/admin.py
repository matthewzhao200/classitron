"""
Register models in the admin interface so we can edit them there.
"""

from django.contrib import admin
from classitron.classify.models import ODLC, Image, Mission

# Register models in the admin interface so we can edit them there.
admin.site.register(ODLC)
admin.site.register(Image)
admin.site.register(Mission)
