# Generated by Django 3.0.3 on 2020-02-19 05:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('classify', '0006_auto_20200206_0208'),
    ]

    operations = [
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='')),
                ('mission', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='classify.Mission')),
            ],
        ),
    ]
